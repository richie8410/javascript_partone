//Variables ouside scope
var hexRed;
var hexGreen;
var hexBlue;
var hexRed2;
var hexGreen2;
var hexBlue2;


//Return a Hex Value from an RGB Value between 0 and 255
function returnHexRed() {
	//Get the value of the slider for Red
  var rgbValRed = document.getElementById("red").value;
	//Return Elements
  var liRed = document.getElementById("hex-red");
  var redval = document.getElementById("rgb-red");
    //Declare a value to be returned and use the slider value (converted).
    hexRed = Number(rgbValRed).toString(16);
    //check that the length of hex is no more than 2 digits
    if (hexRed.length < 2) {
  		//If value is less than 2 add a preceding '0'
     hexRed = "0" + hexRed;
   }
 		 //Show the vaue as an ouput in HTML
      liRed.textContent = "Red: " + hexRed;
      redval.textContent = "Red: " + rgbValRed;
    }


    function returnHexRed2() {
      var rgbValRed = document.getElementById("red2").value;
  //Return Elements
  var liRed = document.getElementById("hex-red2");
  var redval = document.getElementById("rgb-red2");
  hexRed2 = Number(rgbValRed).toString(16);
  if (hexRed2.length < 2) {
   hexRed2 = "0" + hexRed2;
 }
    //Show the vaue as an ouput in HTML
    liRed.textContent = "Red: " + hexRed2;
    redval.textContent = "Red: " + rgbValRed;
  }


//Green
function returnHexGreen() {
  var rgbValGreen = document.getElementById("green").value;
  var liGreen = document.getElementById("hex-green");
  var greenval = document.getElementById("rgb-green");

  hexGreen = Number(rgbValGreen).toString(16);
  if (hexGreen.length < 2) {
    hexGreen = "0" + hexGreen;
  }
 	//Show the vaue as an ouput in HTML
 	liGreen.textContent = "Green: " + hexGreen;
 	greenval.textContent = "Green: " + rgbValGreen;
 }

//Green2
function returnHexGreen2() {
  var rgbValGreen = document.getElementById("green2").value;
  var liGreen = document.getElementById("hex-green2");
  var greenval = document.getElementById("rgb-green2");
  hexGreen2 = Number(rgbValGreen).toString(16);
  if (hexGreen2.length < 2) {
    hexGreen2 = "0" + hexGreen2;
  }
  ///Show the vaue as an ouput in HTML
  liGreen.textContent = "Green: " + hexGreen2;
  greenval.textContent = "Green: " + rgbValGreen;
}




//Blue
function returnHexBlue() {
  var rgbValBlue = document.getElementById("blue").value;
  var liBlue = document.getElementById("hex-blue");
  var blueval = document.getElementById("rgb-blue");
  hexBlue = Number(rgbValBlue).toString(16);
  if (hexBlue.length < 2) {
    hexBlue = "0" + hexBlue;
  }
 	//Show the vaue as an ouput in HTML
 	liBlue.textContent = "Blue: " + hexBlue;
 	blueval.textContent = "Blue: " + rgbValBlue;

 }

//Blue2
function returnHexBlue2() {
  var rgbValBlue = document.getElementById("blue2").value;
  var liBlue = document.getElementById("hex-blue2");
  var blueval = document.getElementById("rgb-blue2");
  hexBlue2 = Number(rgbValBlue).toString(16);
  if (hexBlue2.length < 2) {
    hexBlue2 = "0" + hexBlue2;
  }
  //Show the vaue as an ouput in HTML
  liBlue.textContent = "Blue: " + hexBlue2;
  blueval.textContent = "Blue: " + rgbValBlue;

}

//This function will build a full colour from Red / Green / Blue
function createColour() {
	//Add the current values together to form a colour
	var fullColour = "#" + hexRed + hexGreen + hexBlue;
	var liColour = document.getElementById("hex-colour");
	//Show the vaue as an ouput in HTML
	liColour.style.background = fullColour;
	liColour.textContent = "Colour: " + fullColour;

}

function createColour2() {

  //Add the current values together to form a colour
  var fullColour = "#" + hexRed2 + hexGreen2 + hexBlue2;
  var liColour = document.getElementById("hex-colour2");
  var visualResult = document.getElementById("result2");
  //Show the vaue as an ouput in HTML
  liColour.style.background = fullColour;
  liColour.textContent = "Colour: " + fullColour;
  //Show the colour visually on the screen
  visualResult.style.background = fullColour;

}

//function will take the two chosen colours and average the values to create a new colour
function calculateAverage() {

//Return Element
var returnedAverageHex = document.getElementById("AverageHexColour");

//get the RGBVals for Colour 1
var rgbRed1 = document.getElementById("red").value;
var rgbGreen1 = document.getElementById("green").value;
var rgbBlue1 = document.getElementById("blue").value;

//get the RGB values for colour 2
let rgbRed2 = document.getElementById("red2").value;
var rgbGreen2 = document.getElementById("green2").value;
var rgbBlue2 = document.getElementById("blue2").value;

//calculate the average of Red
let redAverage = (parseInt(rgbRed1) + parseInt(rgbRed2)) / 2;
//Round to the nearest whole number before converting
let redAverageRound = Math.round(redAverage);
let redAverageHex = Number(redAverageRound).toString(16);
if (redAverageHex.length < 2) {
  redAverageHex = "0" + redAverageHex;
}

//Calculate the average of Green
let greenAverage = (parseInt(rgbGreen1) + parseInt(rgbGreen2)) / 2;
//Round the Value to a whole number
let greenAverageRound = Math.round(greenAverage);
let greenAverageHex = Number(greenAverageRound).toString(16);
if (greenAverageHex.length < 2) {
  greenAverageHex = "0" + greenAverageHex;
}

//Calculate the average of blue
let blueAverage = (parseInt(rgbBlue1) + parseInt(rgbBlue2)) / 2;
//Round to the nearest whole number
let blueAverageRound = Math.round(blueAverage);
let blueAverageHex = Number(blueAverageRound).toString(16);
if (blueAverageHex.length < 2) {
  blueAverageHex = "0" + blueAverageHex;
}

//Create the new colour (RGB)
var colourAverage = "#" + redAverage + greenAverage + blueAverage;
console.log(redAverageHex);

//Create the new Hex Colour
var colourAverageHex = "#" + redAverageHex + greenAverageHex + blueAverageHex;
console.log(colourAverageHex);
//Show the vaue as an ouput in HTML
returnedAverageHex.style.background = colourAverageHex;
returnedAverageHex.textContent = "Average: " + colourAverageHex;


}